import Zephr from './zephr';

describe('_mergeAccessDetails', () => {
  it('merges trials', () => {
    expect(new Zephr()._mergeAccessDetails({
      authenticated: false,
      accessDecisions: {},
      entitlements: {},
      credits: {},
      meters: {},
      trials: {
        "abc": {
          creditsUsed: 5
        }
      }
    }, {
      authenticated: true,
      accessDecisions: {},
      entitlements: {},
      credits: {},
      meters: {},
      trials: {
        "def": {
          creditsUsed: 10
        }
      }
    }))
    .toStrictEqual({
      authenticated: true,
      accessDecisions: {},
      entitlements: {},
      credits: {},
      meters: {},
      trials: {
        "abc": {
          creditsUsed: 5
        },
        "def": {
          creditsUsed: 10
        }
      }
    });
  })

  it('merges null trials', () => {
    expect(new Zephr()._mergeAccessDetails({
      authenticated: false,
      accessDecisions: {},
      entitlements: {},
      credits: {},
      meters: {},
    }, {
      authenticated: true,
      accessDecisions: {},
      entitlements: {},
      credits: {},
      meters: {},
    }))
    .toStrictEqual({
      authenticated: true,
      accessDecisions: {},
      entitlements: {},
      credits: {},
      meters: {},
      trials: {}
    });
  })
});

describe('_mergeMeters Tests', () => {
  it('combines all entries from meters', () => {
    const zephr = new Zephr();
    const meters1 = {
      "OFMEjO": { "decrementedInDecision": false, "totalCredits": 1, "remainingCredits": 0 },
      "zvUC1C": { "usedInDecision": false, "totalCredits": 2, "remainingCredits": 2 },
    }
    const meters2 = {
      "dZgJiR": { "usedInDecision": true, "totalCredits": 2, "remainingCredits": 0 },
      "hErIut": { "usedInDecision": false, "totalCredits": 3, "remainingCredits": 2 },
    }
    const expected = {
      "OFMEjO": { "decrementedInDecision": false, "totalCredits": 1, "remainingCredits": 0 },
      "zvUC1C": { "usedInDecision": false, "totalCredits": 2, "remainingCredits": 2 },
      "dZgJiR": { "usedInDecision": true, "totalCredits": 2, "remainingCredits": 0 },
      "hErIut": { "usedInDecision": false, "totalCredits": 3, "remainingCredits": 2 },
    }

    const result = zephr._mergeCreditData(meters1, meters2);
    expect(result).toStrictEqual(expected);
  });

  it("combines entries with the same key to retain best values", () => {
    const zephr = new Zephr();
    const meters1 = {
      "key1": { "decrementedInDecision": false, "usedInDecision": false, "totalCredits": 2, "remainingCredits": 1 },
      "key2": { "decrementedInDecision": true, "usedInDecision": true, "totalCredits": 2, "remainingCredits": 2 },
    }
    const meters2 = {
      "key1": { "decrementedInDecision": true, "usedInDecision": true, "totalCredits": 2, "remainingCredits": 2 },
      "key2": { "decrementedInDecision": false, "usedInDecision": false, "totalCredits": 2, "remainingCredits": 1 },
    }
    const expected = {
      "key1": { "decrementedInDecision": true, "usedInDecision": true, "totalCredits": 2, "remainingCredits": 1 },
      "key2": { "decrementedInDecision": true, "usedInDecision": true, "totalCredits": 2, "remainingCredits": 1 },
    }

    const result = zephr._mergeCreditData(meters1, meters2);
    expect(result).toStrictEqual(expected);
  });

  it("can handle falsy values when combining entries", () => {
    const zephr = new Zephr();
    const meters1 = {
      "key1": {},
      "key2": { "decrementedInDecision": true, "usedInDecision": true, "totalCredits": 2, "remainingCredits": 1 },
      "key3": { "decrementedInDecision": false, "usedInDecision": false, "totalCredits": 0, "remainingCredits": 0 },
      "key4": {},
    }
    const meters2 = {
      "key1": { "decrementedInDecision": true, "usedInDecision": true, "totalCredits": 2, "remainingCredits": 1 },
      "key2": {},
      "key3": {},
      "key4": {},
    }
    const expected = {
      "key1": { "decrementedInDecision": true, "usedInDecision": true, "totalCredits": 2, "remainingCredits": 1 },
      "key2": { "decrementedInDecision": true, "usedInDecision": true, "totalCredits": 2, "remainingCredits": 1 },
      "key3": { "decrementedInDecision": false, "usedInDecision": false, "totalCredits": 0, "remainingCredits": 0 },
      "key4": {},
    }

    const result = zephr._mergeCreditData(meters1, meters2);
    expect(result).toStrictEqual(expected);
  });
});

describe('_minOrNumber', () => {
  it('returns min of two numbers', () => {
    expect(new Zephr()._minOrNumber(2, 5)).toStrictEqual(2);
  });
  it('returns the number when one is not a number', () => {
    expect(new Zephr()._minOrNumber(2, undefined)).toStrictEqual(2);
    expect(new Zephr()._minOrNumber("2", 3)).toStrictEqual(3);
  });
  it('returns undefined when neither is a number', () => {
    expect(new Zephr()._minOrNumber(null, {})).toStrictEqual(undefined);
  });
});

describe('custom fetcher', () => {
  it('uses the provided fetcher', async () => {
    const fetcher = jest.fn((url, options) => {
      expect(url).toBe('https://cdn.zephr.com/zephr/features');
      expect(options).toEqual( {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
        },
      });

      return Promise.resolve({ json: () => [] });
    });
    const zephr = new Zephr('https://cdn.zephr.com', fetcher);
    const result = await zephr.fetchLiveFeatures();

    expect(fetcher).toHaveBeenCalled();
    expect(result).toEqual([]);
  });
});

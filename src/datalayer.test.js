import { addDatalayer } from './datalayer';

describe('applyDataLayer', () => {
    beforeEach(() => {
        window.Zephr = {};
        delete window.testDatalayer;
    });

    it('feature outcomes', () => {
        window.Zephr.outcomes = {
            'feature-1': {
                featureLabel: 'Feature 1',
                outcomeId: 'outcome-1',
                outcomeLabel: 'outcome 1'
            }
        }
        addDatalayer({
            datalayerName: 'testDatalayer',
            includeOutcomes: true,
            outcomesAsEvents: true,
            groupFields: false
        })
        expect(window.testDatalayer).toStrictEqual(
            [
                {
                    event: 'zephr-pageview',
                    zephrOutcomes: {
                        'feature-1': {
                            featureLabel: 'Feature 1',
                            outcomeId: 'outcome-1',
                            outcomeLabel: 'outcome 1'
                        }
                    }
                },
                {
                    event: 'zephr-outcome-feature-1',
                    featureId: 'feature-1',
                    featureLabel: 'Feature 1',
                    outcomeId: 'outcome-1',
                    outcomeLabel: 'outcome 1',
                }
            ]
        )
    });

    it('trials', () => {
        window.Zephr.accessDetails = {
            trials: {
                'trial-1': {
                    reportInDataLayer: true,
                    totalCredits: 5,
                    remainingCredits: 2,
                    dataLayerCreditsUsedKey: 'trial-1-used-credits',
                    dataLayerCreditsRemainingKey: 'trial-1-remaining-credits'
                },
                'trial-2': {
                    reportInDataLayer: true,
                    totalCredits: 2,
                    remainingCredits: 0,
                    dataLayerCreditsUsedKey: 'trial-2-used-credits',
                    dataLayerCreditsRemainingKey: 'trial-2-remaining-credits'
                },
                'trial-3': {
                    reportInDataLayer: false,
                    totalCredits: 5,
                    remainingCredits: 5,
                    dataLayerCreditsUsedKey: 'trial-3-used-credits',
                    dataLayerCreditsRemainingKey: 'trial-3-remaining-credits'
                }
            }
        }
        addDatalayer({
            datalayerName: 'testDatalayer',
            includeOutcomes: false,
            outcomesAsEvents: false,
            groupFields: false
        })
        expect(window.testDatalayer).toStrictEqual(
            [
                {
                    event: 'zephr-pageview',
                    'trial-1-used-credits': 3,
                    'trial-1-remaining-credits': 2,
                    'trial-2-used-credits': 2,
                    'trial-2-remaining-credits': 0
                }
            ]
        )
    });
    it('trials grouped', () => {
        window.Zephr.accessDetails = {
            trials: {
                'trial-1': {
                    reportInDataLayer: true,
                    totalCredits: 5,
                    remainingCredits: 2,
                    dataLayerCreditsUsedKey: 'trial-1-used-credits',
                    dataLayerCreditsRemainingKey: 'trial-1-remaining-credits'
                },
                'trial-2': {
                    reportInDataLayer: true,
                    totalCredits: 2,
                    remainingCredits: 0,
                    dataLayerCreditsUsedKey: 'trial-2-used-credits',
                    dataLayerCreditsRemainingKey: 'trial-2-remaining-credits'
                },
                'trial-3': {
                    reportInDataLayer: false,
                    totalCredits: 5,
                    remainingCredits: 5,
                    dataLayerCreditsUsedKey: 'trial-3-used-credits',
                    dataLayerCreditsRemainingKey: 'trial-3-remaining-credits'
                }
            }
        }
        addDatalayer({
            datalayerName: 'testDatalayer',
            includeOutcomes: false,
            outcomesAsEvents: false,
            groupFields: true
        })
        expect(window.testDatalayer).toStrictEqual(
            [
                {
                    event: 'zephr-pageview',
                    zephrTrials: {
                        'trial-1-used-credits': 3,
                        'trial-1-remaining-credits': 2,
                        'trial-2-used-credits': 2,
                        'trial-2-remaining-credits': 0
                    }
                }
            ]
        )
    });
    it('trial tracking details', () => {
        window.Zephr.accessDetails = {
            trialTrackingDetails: [
                {
                    entitlementType: 'credits',
                    entitlementId: 'credit-1',
                    creditsRemainingKey: 'credit-1-remaining-credits',
                    creditsUsedKey: 'credit-1-used-credits'
                },
                {
                    entitlementType: 'meters',
                    entitlementId: 'meter-1',
                    creditsRemainingKey: 'meter-1-remaining-credits',
                    creditsUsedKey: 'meter-1-used-credits'
                }
            ],
            credits: {
                'credit-1': {
                    remainingCredits: 3,
                    totalCredits: 5
                }
            },
            meters: {
                'meter-1': {
                    remainingCredits: 2,
                    totalCredits: 5
                }
            }
        }
        addDatalayer({
            datalayerName: 'testDatalayer',
            includeOutcomes: false,
            outcomesAsEvents: false,
            groupFields: false
        })
        expect(window.testDatalayer).toStrictEqual(
            [
                {
                    event: 'zephr-pageview',
                    'credit-1-remaining-credits': 3,
                    'credit-1-used-credits': 2,
                    'meter-1-remaining-credits': 2,
                    'meter-1-used-credits': 3
                }
            ]
        )
    });
    it('trial tracking details grouped', () => {
        window.Zephr.accessDetails = {
            trialTrackingDetails: [
                {
                    entitlementType: 'credits',
                    entitlementId: 'credit-1',
                    creditsRemainingKey: 'credit-1-remaining-credits',
                    creditsUsedKey: 'credit-1-used-credits'
                },
                {
                    entitlementType: 'meters',
                    entitlementId: 'meter-1',
                    creditsRemainingKey: 'meter-1-remaining-credits',
                    creditsUsedKey: 'meter-1-used-credits'
                }
            ],
            credits: {
                'credit-1': {
                    remainingCredits: 3,
                    totalCredits: 5
                }
            },
            meters: {
                'meter-1': {
                    remainingCredits: 2,
                    totalCredits: 5
                }
            }
        }
        addDatalayer({
            datalayerName: 'testDatalayer',
            includeOutcomes: false,
            outcomesAsEvents: false,
            groupFields: true
        })
        expect(window.testDatalayer).toStrictEqual(
            [
                {
                    event: 'zephr-pageview',
                    zephrTrials: {
                        'credit-1-remaining-credits': 3,
                        'credit-1-used-credits': 2,
                        'meter-1-remaining-credits': 2,
                        'meter-1-used-credits': 3
                    }
                }
            ]
        )
    });
    it('trial tracking details missing meter', () => {
        window.Zephr.accessDetails = {
            trialTrackingDetails: [
                {
                    entitlementType: 'credits',
                    entitlementId: 'credit-1',
                    creditsRemainingKey: 'credit-1-remaining-credits',
                    creditsUsedKey: 'credit-1-used-credits'
                },
                {
                    entitlementType: 'meters',
                    entitlementId: 'meter-1',
                    creditsRemainingKey: 'meter-1-remaining-credits',
                    creditsUsedKey: 'meter-1-used-credits'
                }
            ],
            credits: {
                'credit-1': {
                    remainingCredits: 3,
                    totalCredits: 5
                }
            }
        }
        addDatalayer({
            datalayerName: 'testDatalayer',
            includeOutcomes: false,
            outcomesAsEvents: false,
            groupFields: false
        })
        expect(window.testDatalayer).toStrictEqual(
            [
                {
                    event: 'zephr-pageview',
                    'credit-1-remaining-credits': 3,
                    'credit-1-used-credits': 2
                }
            ]
        )
    });
});
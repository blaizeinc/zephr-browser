const parseTransformation = (transformation) => {
  switch (transformation.type) {
    case 'LEAVE_PRISTINE':
      return {
        type: 'LeavePristine',
      };
    case 'FORM':
      return {
        type: 'Form',
        id: transformation.id,
      };
    case 'PAYMENT_FORM':
      return {
        type: 'PaymentForm',
        id: transformation.id,
      };
    case 'RESOURCE':
    case 'PARAMETERISED_RESOURCE':
      return {
        type: 'UIComponent',
        componentId: transformation.id,
      };
    case 'URL':
      return {
        type: 'HostedUIComponent',
        url: transformation.url,
      };
    case 'COMPONENT_TEMPLATE':
      return {
        type: 'ComponentTemplate',
        templateId: transformation.id,
      };
    case 'PARAMETERISED_COMPONENT_TEMPLATE':
      return {
        type: 'PARAMETERISED_COMPONENT_TEMPLATE',
        templateId: transformation.id,
      };
    case 'TRUNCATE':
      return {
        type: 'Truncate',
        truncateLength: transformation.contentLength,
        style: 'nostyle'
      };
    case 'TRUNCATE_WITH_STYLE':
      return {
        type: 'Truncate',
        truncateLength: transformation.contentLength,
        style: transformation.style,
      };
    case 'OUTCOME_TRACKER':
      return {
        type: 'OutcomeTracker',
        featureId: transformation.featureId,
        featureLabel: transformation.featureLabel,
        transformationId: transformation.transformationId,
        transformationLabel: transformation.transformationLabel,
      };
  }
};

const parseRuleTransformations = (ruleTransformations) => {
  return ruleTransformations.map(transformation => parseTransformation(transformation));
};

const parseZoneRuleTransformations = (zoneRuleTransformations) => {
  return zoneRuleTransformations.map(zone => ({
    type: 'Zone',
    id: zone.zone,
    contents: parseRuleTransformations(zone.ruleTransformations)
  }));
};

const parseFeatureResults = (featureResults) => {
  if (!featureResults) return {};

  const parsedResults = {};
  parsedResults.ruleTransformations = parseRuleTransformations(featureResults.ruleTransformations || []);
  parsedResults.zoneRuleTransformations = parseZoneRuleTransformations(featureResults.zoneRuleTransformations || []);
  return parsedResults;
};

export {
  parseFeatureResults
};

import { applyTransformations } from './transformations';

const basicTestHtml = `
  <div id="test">
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      Sagittis eu volutpat odio facilisis mauris.
    </p>
  </div>
`;

const basicTestHtmlWithScript = `
  <div id="test">
    <h3>Text</h3>
    <script>console.log('script executed');</script>
  </div>
`;

const nestedTestHtml = `
  <div>
    <h1>Sandwich</h1>
    ${basicTestHtml}
    <footer>copyright</footer>
  </div>
`;
const repeatedTestHtml = `
  <div>
    <p class="test">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sagittis eu volutpat odio facilisis mauris.</p>
    <p class="test">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sagittis eu volutpat odio facilisis mauris.</p>
    <p class="test">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sagittis eu volutpat odio facilisis mauris.</p>
  </div>
`;
const formHtml = `
  <div>
    <form><input type="submit"></form>
    <script>console.log('script execution');</script>
  </div>
`;
const componentHtml = `
  <article>
    <h1>Paywall</h1>
  </article>
`;
const headerHtml = `
  <div class="zephr-zoned-transformation" id="zephr-zone-header">
  </div>
`;

const componentInHeaderHtml = `
  <div class="zephr-zoned-transformation" id="zephr-zone-header">
    ${componentHtml}
  </div>
`;

const styleScriptComponentHtml = `
  <style>h1 { color: red; }</style>
  <h1>Paywall</h1>
  <script type="application/javascript">console.log("hello paywall")</script>
`;
const expectedTruncation = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';

const nypTestHtml = `
  <div class="single__content entry-content m-bottom entry-content-exclusive">
    <div class="single__inline-module inline-module inline-module--newsletter alignright">
      <div class="inline-module__inner">
        <div class="inline-module__icon">
          <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40"><circle cx="20" cy="20" r="20" fill="#C60800"></circle><g fill="#fff"><path d="M9 13v14.414h22V13l-11 9.1Z" data-name="envelope"></path><path d="M11.3 13h17.864l-8.933 6.909z" data-name="top flap #FFFFFF"></path></g></svg>
        </div>
        <h3 class="inline-module__title headline headline--combo-sm-md">
          Delivering insights on all things Amazin's		</h3>
        <p class="inline-module__cta">
          Sign up for Inside the Mets by Mike Puma, exclusively on Sports+		</p>
        <form class="inline-module__form newsletter-form" data-nypost-editor="newsletter-block">
          <div class="newsletter-form__wrapper">
            <div class="floating-label input-container input-container--floating-label">
              <input type="hidden" name="site_id" value="nypost">
              <input type="hidden" id="_newsletter_nonce" name="_newsletter_nonce" value="65c2572fd2"><input type="hidden" name="_wp_http_referer" value="/2022/10/14/mets-have-to-be-patient-to-collect-on-pitching-prospects/">					<input type="hidden" name="list_id" value="88882">
              <input type="email" id="newsletter-block-email-635180c46fd1f" class="floating-label__input " name="email" placeholder=" ">
              <div class="floating-label__label-wrap">
                <label for="email" class="floating-label__label">Enter your email address</label>
              </div>
            </div>
            <p class="newsletter-form__error">
              <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15"><path d="M7.5 0A7.5 7.5 0 1 0 15 7.5 7.5 7.5 0 0 0 7.5 0Zm.62 3.952-.112 4.15a.516.516 0 0 1-.508.534.516.516 0 0 1-.508-.534l-.112-4.15a.621.621 0 0 1 .62-.635.621.621 0 0 1 .625.635Zm-.62 7.082a.664.664 0 1 1 .689-.663.67.67 0 0 1-.689.663Z" fill="#e13131"></path></svg>
              <span class="error_msg">
                Please provide a valid email address.					</span>
            </p>
            <input type="submit" value="Sign Up" class="submit" data-ga-event="
              {&quot;category&quot;:&quot;Inline Newsletter Module Membership - Inside the Mets&quot;,&quot;action&quot;:&quot;Button Click&quot;,&quot;label&quot;:&quot;https:\/\/nypost.com\/2022\/10\/14\/mets-have-to-be-patient-to-collect-on-pitching-prospects\/&quot;}					">
            <div class="newsletter-form__notice">
              <p class="t-color-black m-bottom-none">
                By clicking above you agree to the 						<a href="/terms">Terms of Use</a>
                and 						<a href="/privacy">Privacy Policy</a>.
              </p>
            </div>
          </div>
          <div class="newsletter-form__success">
            <p class="inline-module__cta">
              <strong>Thank you</strong>
              <br> Enjoy this Post Sports+ exclusive newsletter!				</p>
            <div class="t-center">
              <a href="https://email.nypost.com/">
                <span>Check out more newsletters</span>
              </a>
            </div>
          </div>
          <input type="checkbox" name="nyp_phone" value="1" style="display:none !important" tabindex="-1" autocomplete="off">
        </form>
      </div>
    </div>
  </div>
`;

const expectedNYPTruncation = `
  <div class="single__content entry-content m-bottom entry-content-exclusive">
    <div class="single__inline-module inline-module inline-module--newsletter alignright">
      <div class="inline-module__inner">
        <div class="inline-module__icon">
          <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40"><circle cx="20" cy="20" r="20" fill="#C60800"></circle><g fill="#fff"><path d="M9 13v14.414h22V13l-11 9.1Z" data-name="envelope"></path><path d="M11.3 13h17.864l-8.933 6.909z" data-name="top flap #FFFFFF"></path></g></svg>
        </div>
        <h3 class="inline-module__title headline headline--combo-sm-md">
          Delivering insights on all things Amazin's		</h3>
        <p class="inline-module__cta">
          Sign up for Inside the Mets by Mike Puma, exclusively on Sports+		</p>
        <form class="inline-module__form newsletter-form" data-nypost-editor="newsletter-block">
          <div class="newsletter-form__wrapper">
            <div class="floating-label input-container input-container--floating-label">
              <input type="hidden" name="site_id" value="nypost">
              <input type="hidden" id="_newsletter_nonce" name="_newsletter_nonce" value="65c2572fd2"><input type="hidden" name="_wp_http_referer" value="/2022/10/14/mets-have-to-be-patient-to-collect-on-pitching-prospects/">					<input type="hidden" name="list_id" value="88882">
              <input type="email" id="newsletter-block-email-635180c46fd1f" class="floating-label__input " name="email" placeholder=" ">
              <div class="floating-label__label-wrap">
                <label for="email" class="floating-label__label">Enter your email address</label>
              </div>
            </div>
            <p class="newsletter-form__error">
              <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15"><path d="M7.5 0A7.5 7.5 0 1 0 15 7.5 7.5 7.5 0 0 0 7.5 0Zm.62 3.952-.112 4.15a.516.516 0 0 1-.508.534.516.516 0 0 1-.508-.534l-.112-4.15a.621.621 0 0 1 .62-.635.621.621 0 0 1 .625.635Zm-.62 7.082a.664.664 0 1 1 .689-.663.67.67 0 0 1-.689.663Z" fill="#e13131"></path></svg>
              <span class="error_msg">
                Please provide a valid email address.					</span>
            </p>
          </div>
        </form>
      </div>
    </div>
    <div style="position: absolute; height: 100%; width: 100%; bottom: 0px;"></div>
  </div>
`;

const testHtmlSingleWordTag = `
  <div class="test">
    <p>
      Lorem ipsum <a href="#">dolor</a> sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </p>
  </div>
`;

const testHtmlSingleWordTagExpected = `
  <div class="test">
    <p>
      Lorem ipsum <a href="#">dolor</a> sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    </p>
    <div style="position: absolute; height: 100%; width: 100%; bottom: 0px;"></div>
  </div>
`;

const uiComponentMixedTagsHtml = `
<style></style>Hello <br /> Goodbye
`;

const testHtmlJapanese = `
  <div class="test">
    <p>
      大谷翔平は、日本のプロ野球選手であり、2018年からMLBのロサンゼルス・エンゼルスでプレーしています。彼は投手としても打者としても非凡な才能を持ち、“二刀流“として知られています。投手としては速球や変化球を駆使し、打者としてはパワフルなバッティングを誇ります。彼の活躍は日本国内だけでなく、世界中の野球ファンに注目されています。
    </p>
  </div>
`;

const expectedHtmlJapaneseTruncation = `
  <div class="test">
    <p>
      大谷翔平は、日本のプロ野球選手であり、2018年からMLBのロサンゼルス・エンゼルスでプレーしています。
    </p>
  </div>
`

describe('applyTransformations Tests', () => {
  let consoleOutput = [];
  const originalLog = console.log;
  const mockedLog = output => consoleOutput.push(output);

  beforeEach(() => {
    document.body.innerHTML = basicTestHtml;
    delete window.Zephr;
    console.log = mockedLog;
  });

  afterEach(() => {
    consoleOutput = [];
    console.log = originalLog;
  });

  it('LeavePristine', () => {
    applyTransformations(
      document.querySelectorAll('#test'),
      [{ type: 'LeavePristine' }],
      [],
    );
    expect(document.body.innerHTML.trim()).toEqual(basicTestHtml.trim());
  });

  it('LeavePristine - Nested', () => {
    document.body.innerHTML = nestedTestHtml;
    applyTransformations(
      document.querySelectorAll('#test'),
      [{ type: 'LeavePristine' }],
      [],
    );
    expect(document.querySelector('#test').outerHTML.trim()).toEqual(basicTestHtml.trim());
  });

  it('LeavePristine - Event listener preserved', () => {
    document.querySelector('#test').addEventListener('click', () => console.log('Clicked!'));
    document.querySelector('#test').click();
    applyTransformations(
      document.querySelectorAll('#test'),
      [{ type: 'LeavePristine' }],
      [],
    );
    document.querySelector('#test').click();
    expect(document.body.innerHTML.trim()).toEqual(basicTestHtml.trim());
    expect(consoleOutput).toEqual(['Clicked!', 'Clicked!']);
  });

  it('Script within Feature does not execute twice', () => {
    document.body.innerHTML = basicTestHtmlWithScript;
    const scripts = document.body.getElementsByTagName('script');
    for (let n = 0; n < scripts.length; n++) {
      eval(scripts[n].innerHTML);
    }

    applyTransformations(
      document.querySelectorAll('#test'),
      [{ type: 'LeavePristine' }],
      [],
    );
    expect(document.body.innerHTML.replace(/\s/g, ''))
      .toEqual(basicTestHtmlWithScript.replace(/\s/g, ''));
    expect(consoleOutput).toEqual(['script executed']);
  });

  it('Remove', () => {
    applyTransformations(
      document.querySelectorAll('#test'),
      [{ type: 'Remove' }],
      [],
    );
    expect(document.body.innerHTML.trim()).toEqual('');
  });

  it('Truncate - No Style', () => {
    applyTransformations(
      document.querySelectorAll('#test'),
      [{ type: 'Truncate', truncateLength: 15, style: 'nostyle' }],
      [],
    );
    expect(document.querySelector('#test p').innerHTML.trim())
      .toEqual(expectedTruncation);
  });

  it('Truncate - Multiple - No Style', () => {
    document.body.innerHTML = repeatedTestHtml;
    applyTransformations(
      document.querySelectorAll('.test'),
      [{ type: 'Truncate', truncateLength: 15, style: 'nostyle' }],
      [],
    );

    const nodes = document.querySelectorAll('.test');
    expect(nodes.length).toEqual(3);
    expect(nodes[0].innerHTML.trim()).toEqual(expectedTruncation);
    expect(nodes[1].innerHTML.trim()).toEqual(expectedTruncation);
    expect(nodes[2].innerHTML.trim()).toEqual(expectedTruncation);
  });

  it('Truncate - Line Break', () => {
    applyTransformations(
      document.querySelectorAll('#test'),
      [{ type: 'Truncate', truncateLength: 15, style: 'linebreak' }],
      [],
    );
    expect(document.querySelector('#test p').innerHTML.trim())
      .toEqual(expectedTruncation);
    expect(document.querySelector('#test hr')).toBeTruthy();
  });

  it('Truncate - Line Break - No truncate', () => {
    applyTransformations(
      document.querySelectorAll('#test'),
      [{ type: 'Truncate', truncateLength: 25, style: 'linebreak' }],
      [],
    );
    expect(document.querySelector('#test').outerHTML.replace(/\s/g, ''))
      .toEqual(basicTestHtml.replace(/\s/g, ''));
    expect(document.querySelector('#test hr')).toBeNull();
  });

  it('Truncate - Fade Out', () => {
    applyTransformations(
      document.querySelectorAll('#test'),
      [{ type: 'Truncate', truncateLength: 15, style: 'fadeout' }],
      [],
    );
    expect(document.querySelector('#test p').innerHTML.trim())
      .toEqual(expectedTruncation);
    expect(document.querySelector('div').style.position).toEqual('relative');
    expect(document.querySelector('div > #test p + div').style.position).toEqual('absolute');
  });

  it('Truncate - Fade Out - NYP', () => {
    document.body.innerHTML = nypTestHtml;
    applyTransformations(
      document.querySelectorAll('.entry-content'),
      [{ type: 'Truncate', truncateLength: 25, style: 'fadeout' }],
      [],
    );
    expect(document.querySelector('.entry-content').outerHTML.replace(/\s/g, ''))
      .toEqual(expectedNYPTruncation.replace(/\s/g, ''));
  });

  it('Truncate - Fade Out - NYP - No truncate', () => {
    document.body.innerHTML = nypTestHtml;
    applyTransformations(
      document.querySelectorAll('.entry-content'),
      [{ type: 'Truncate', truncateLength: 100, style: 'fadeout' }],
      [],
    );
    expect(document.querySelector('.entry-content').outerHTML.replace(/\s/g, ''))
      .toEqual(nypTestHtml.replace(/\s/g, ''));
  });

  it('Truncate - Fade Out - nested tag with single word ', () => {
    document.body.innerHTML = testHtmlSingleWordTag;
    applyTransformations(
      document.querySelectorAll('.test'),
      [{ type: 'Truncate', truncateLength: 19, style: 'fadeout' }],
      [],
    );
    expect(document.querySelector('.test').outerHTML.replace(/\s/g, ''))
      .toEqual(testHtmlSingleWordTagExpected.replace(/\s/g, ''));
  });

  it('Truncate - Locale Aware', () => {
    document.body.innerHTML = testHtmlJapanese;
    applyTransformations(
      document.querySelectorAll('.test'),
      [{ type: 'Truncate', truncateLength: 15, style: 'nostyle' }],
      [],
      {},
      { LOCALE_AWARE_TRUNCATION: true },
    );
    expect(document.querySelector('.test').outerHTML.replace(/\s/g, ''))
      .toEqual(expectedHtmlJapaneseTruncation.replace(/\s/g, ''));
  });

  it('Outcome Tracker', () => {
    applyTransformations(
      document.querySelectorAll('#test'),
      [{
        type: 'OutcomeTracker',
        featureId: 'test',
        featureLabel: 'Test',
        outcomeId: 'outcome1',
        outcomeLabel: 'Outcome 1',
      }],
      []
    );
    expect(document.body.innerHTML.trim()).toEqual('');
    expect(window.Zephr.outcomes)
      .toStrictEqual({
        'test': {
          featureLabel: 'Test',
          outcomeId: 'outcome1',
          outcomeLabel: 'Outcome 1',
        },
      });
  });

  it('Form', () => {
    applyTransformations(
      document.querySelectorAll('#test'),
      [{
        type: 'Form',
        formId: 'test',
      }],
      [],
      {
        forms: { test: formHtml },
      },
    );
    expect(document.querySelector('#test')).toBeNull();
    expect(document.body.innerHTML.replace(/\s/g, ''))
      .toEqual(formHtml.replace(/\s/g, ''));
    expect(consoleOutput).toEqual(['script execution']);
  });

  it('Payment Form', () => {
    applyTransformations(
      document.querySelectorAll('#test'),
      [{
        type: 'PaymentForm',
        formId: 'test',
      }],
      [],
      {
        paymentForms: { test: formHtml },
      },
    );
    expect(document.querySelector('#test')).toBeNull();
    expect(document.body.innerHTML.replace(/\s/g, ''))
      .toEqual(formHtml.replace(/\s/g, ''));
    expect(consoleOutput).toEqual(['script execution']);
  });

  it('UI Component Rendering with Mixed Tags', () => {
    applyTransformations(
      document.querySelectorAll('#test'),
      [{
        type: 'UIComponent',
        componentId: 'test',
      }],
      [],
      {
        uiComponents: { test: uiComponentMixedTagsHtml },
      },
    );

    expect(document.querySelector('#test')).toBeNull();

    // Normalize both expected and received strings to ensure <br/> is treated the same as <br>
    const normalizedReceived = document.body.innerHTML.replace(/\s/g, '').replace(/<br\/>/g, '<br>');
    const normalizedExpected = uiComponentMixedTagsHtml.replace(/\s/g, '').replace(/<br\/>/g, '<br>');

    expect(normalizedReceived).toEqual(normalizedExpected);
  });

  it('UI Component', () => {
    applyTransformations(
      document.querySelectorAll('#test'),
      [{
        type: 'UIComponent',
        componentId: 'test',
      }],
      [],
      {
        uiComponents: { test: componentHtml },
      },
    );
    expect(document.querySelector('#test')).toBeNull();
    expect(document.body.innerHTML.replace(/\s/g, ''))
      .toEqual(componentHtml.replace(/\s/g, ''));
  });

  it('UI Component (plaintext)', () => {
    const plaintext = 'Hello World.';
    applyTransformations(
      document.querySelectorAll('#test'),
      [{
        type: 'UIComponent',
        componentId: 'test',
      }],
      [],
      {
        uiComponents: { test: plaintext },
      },
    );
    expect(document.querySelector('#test')).toBeNull();
    expect(document.body.innerHTML.replace(/\s/g, ''))
      .toEqual(plaintext.replace(/\s/g, ''));
  });

  it('Hosted UI Component', () => {
    applyTransformations(
      document.querySelectorAll('#test'),
      [{
        type: 'HostedUIComponent',
        url: 'http://test.com',
      }],
      [],
      {
        hostedUiComponents: { 'http://test.com': componentHtml },
      },
    );
    expect(document.querySelector('#test')).toBeNull();
    expect(document.body.innerHTML.replace(/\s/g, ''))
      .toEqual(componentHtml.replace(/\s/g, ''));
  });

  it('Component Template', () => {
    applyTransformations(
      document.querySelectorAll('#test'),
      [{
        type: 'ComponentTemplate',
        componentId: 'test',
      }],
      [],
      {
        componentTemplates: { test: componentHtml },
      },
    );
    expect(document.querySelector('#test')).toBeNull();
    expect(document.body.innerHTML.replace(/\s/g, ''))
      .toEqual(componentHtml.replace(/\s/g, ''));
  });

  it('Component Template (style + script)', () => {
    applyTransformations(
      document.querySelectorAll('#test'),
      [{
        type: 'ComponentTemplate',
        componentId: 'test',
      }],
      [],
      {
        componentTemplates: { test: styleScriptComponentHtml },
      },
    );
    expect(document.querySelector('#test')).toBeNull();
    expect(document.body.innerHTML.replace(/\s/g, ''))
      .toEqual(styleScriptComponentHtml.replace(/\s/g, ''));
  });

  it('Multiple Transformations - Truncate + Outcome Tracker', () => {
    applyTransformations(
      document.querySelectorAll('#test'),
      [{
        type: 'Truncate',
        truncateLength: 15,
        style: 'nostyle',
      }, {
        type: 'OutcomeTracker',
        featureId: 'test',
        featureLabel: 'Test',
        outcomeId: 'outcome1',
        outcomeLabel: 'Outcome 1',
      }],
      [],
    );
    expect(document.querySelector('#test p').innerHTML.trim())
      .toEqual(expectedTruncation);
    expect(window.Zephr.outcomes)
      .toStrictEqual({
        'test': {
          featureLabel: 'Test',
          outcomeId: 'outcome1',
          outcomeLabel: 'Outcome 1',
        },
      });
  });

  it('Multiple Transformations - Form + LeavePristine + Outcome Tracker', () => {
    applyTransformations(
      document.querySelectorAll('#test'),
      [{
        type: 'Form',
        formId: 'test',
      }, {
        type: 'Truncate',
        truncateLength: 15,
        style: 'nostyle',
      }, {
        type: 'OutcomeTracker',
        featureId: 'test',
        featureLabel: 'Test',
        outcomeId: 'outcome1',
        outcomeLabel: 'Outcome 1',
      }],
      [],
      {
        forms: { test: formHtml },
      },
    );


    expect(document.querySelector('div form')).toBeTruthy();
    expect(document.querySelector('div script')).toBeTruthy();
    expect(document.querySelector('div + #test')).toBeTruthy();
    expect(document.querySelector('#test p').innerHTML.trim())
      .toEqual(expectedTruncation);
    expect(window.Zephr.outcomes)
      .toStrictEqual({
        'test': {
          featureLabel: 'Test',
          outcomeId: 'outcome1',
          outcomeLabel: 'Outcome 1',
        },
      });
  });

  it('Zone', () => {
    applyTransformations(
      document.querySelectorAll('#test'),
      [],
      [{
        type: 'Zone',
        id: 'HEADER',
        contents: [
          {
            type: 'UIComponent',
            componentId: 'test',
          }
        ]
      }],
      {
        uiComponents: {
          HEADER: headerHtml,
          test: componentHtml
        },
      },
    );
    expect(document.querySelector('#test')).toBeNull();
    expect(document.body.innerHTML.replace(/\s/g, ''))
      .toEqual(componentInHeaderHtml.replace(/\s/g, ''));
  });
});

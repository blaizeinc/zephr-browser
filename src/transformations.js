import { DEFAULT_FLAGS, isIntlSegmenterSupported, parseFromString, executeScriptTags, hasTagsContainingWord } from './utils';
import { truncateNodeLegacy, truncateNodeLocaleAware } from './outcomeNodes'

const addOutcomeTrackerScript = (transformation) => {
  if (!window.Zephr) window.Zephr = {};
  if (!window.Zephr.outcomes) window.Zephr.outcomes = {};

  window.Zephr.outcomes[transformation.featureId] = {
    featureLabel: transformation.featureLabel,
    outcomeId: transformation.outcomeId,
    outcomeLabel: transformation.outcomeLabel,
  };
};

const injectForm = (transformation, resources) => {
  if (!resources.forms || !resources.forms[transformation.formId]) {
    console.error('Form not found.');
    return;
  }

  return parseFromString(resources.forms[transformation.formId]);
};

const injectPaymentForm = (transformation, resources) => {
  if (!resources.paymentForms || !resources.paymentForms[transformation.formId]) {
    console.error('Payment form not found.');
    return;
  }

  return parseFromString(resources.paymentForms[transformation.formId]);
};

const injectUIComponent = (transformation, resources) => {
  if (!resources.uiComponents || !resources.uiComponents[transformation.componentId]) {
    console.error('UI component not found.');
    return;
  }

  return parseFromString(resources.uiComponents[transformation.componentId]);
};

const injectZone = (id, contents, resources) => {
  if (!resources.uiComponents || !resources.uiComponents[id]) {
    console.error('Zone not found.');
    return;
  }

  const zone = parseFromString(resources.uiComponents[id]);
  const zoneElement = zone.getElementById(`zephr-zone-${id.toLowerCase()}`);
  contents.forEach(c => zoneElement.appendChild(c));
  return zone;
};

const injectHostedUIComponent = (transformation, resources) => {
  if (!resources.hostedUiComponents || !resources.hostedUiComponents[transformation.url]) {
    console.error('Hosted UI component not found.');
    return;
  }

  return parseFromString(resources.hostedUiComponents[transformation.url]);
};

const injectComponentTemplate = (transformation, resources) => {
  if (!resources.componentTemplates || !resources.componentTemplates[transformation.componentId]) {
    console.error('Component template not found.');
    return;
  }

  return parseFromString(resources.componentTemplates[transformation.componentId]);
};

const getTransformationNode = (originalNode, transformation, resources, flags) => {
  switch(transformation.type) {
    case 'LeavePristine':
      return originalNode;
    case 'Truncate':
      return (flags.LOCALE_AWARE_TRUNCATION && isIntlSegmenterSupported())
      ? truncateNodeLocaleAware(originalNode, transformation)
      : truncateNodeLegacy(originalNode, transformation);
    case 'OutcomeTracker':
      addOutcomeTrackerScript(transformation);
      break;
    case 'Form':
      return injectForm(transformation, resources);
    case 'PaymentForm':
      return injectPaymentForm(transformation, resources);
    case 'UIComponent':
      return injectUIComponent(transformation, resources);
    case 'HostedUIComponent':
      return injectHostedUIComponent(transformation, resources);
    case 'ComponentTemplate':
      return injectComponentTemplate(transformation, resources);
    case 'Remove':
      break;
    case 'Zone':
      const contents = transformation.contents.map(c => getTransformationNode(originalNode, c, resources, flags));
      return injectZone(transformation.id, contents, resources);
    default:
      console.error(`No matching transformation type ${transformation.type}`);
  }
};

/**
 * Applies the transformations of a feature decision to an array of DOM nodes
 *
 * @param {NodeList} featureNodes - The DOM nodes the transformations should be applied to.
 * @param {Array} transformations - The transformations of the decision.
 * @param {Object} resources - The resources referenced in transformations such as forms, UI components, etc.
 * @param {FeatureDecisionFlags} flags - The flags returned by a feature decision.
 */
const applyTransformations = (
  featureNodes,
  transformations,
  zonedTransformations,
  resources = {},
  flags = DEFAULT_FLAGS,
) => {
  featureNodes.forEach((node) => {
    const parentNode = node.parentNode;
    if (!parentNode) {
      console.warn('Skipping transformation for node without a parent:', node);
      return;
    }
    const nodeIndex = Array.from(parentNode.children).indexOf(node);
    const originalNode = parentNode.removeChild(node);

    const transformationNodes = [...transformations, ...zonedTransformations]
      .map((transformation) => getTransformationNode(originalNode, transformation, resources, flags))
      .filter(Boolean);

    const transformationFragment = document.createDocumentFragment();
    transformationNodes.forEach((outcomeNode) => transformationFragment.appendChild(outcomeNode));

    (nodeIndex < parentNode.children.length)
      ? parentNode.insertBefore(transformationFragment, parentNode.children[nodeIndex])
      : parentNode.appendChild(transformationFragment);
  });
};

export {
  applyTransformations
};

/**
 * @typedef {Object} FeatureDecisionFlags
 * @property {boolean} LOCALE_AWARE_TRUNCATION
 */

/**
 * @type {FeatureDecisionFlags} DEFAULT_FLAGS
 */
const DEFAULT_FLAGS = Object.freeze({
  LOCALE_AWARE_TRUNCATION: false
});

/**
 * Checks if Intl.Segmenter is currently supported.
 *
 * @returns {boolean}
 */
const isIntlSegmenterSupported = () => {
  return Intl && 'Segmenter' in Intl;
}

const createEvent = (eventName) => {
  let event;

  if (typeof(Event) === 'function') {
      event = new Event(eventName);
  } else {
      event = document.createEvent('Event');
      event.initEvent(eventName, true, true);
  }

  return event;
};

const parseFromString = (string) => {
  const parsedHtml = new DOMParser().parseFromString(string, 'text/html');
  // Use childNodes instead of children to capture all nodes including text nodes
  const parsedNodes = [...Array.from(parsedHtml.head.childNodes), ...Array.from(parsedHtml.body.childNodes)];
  if (!parsedNodes.length) {
    return document.createTextNode(string);
  }

  const fragment = document.createDocumentFragment();
  parsedNodes.forEach((parsedNode) => fragment.appendChild(parsedNode));

  executeScriptTags(fragment);

  return fragment;
}

const executeScriptTags = (documentFragment) => {
  const tmpScripts = documentFragment.querySelectorAll('script');
  if (!tmpScripts.length) return;

  const scripts = Array.from(tmpScripts);
  scripts.map((script) => {
    const newScript = document.createElement('script');
    Array.from(script.attributes).forEach((attr) => {
      newScript.setAttribute(attr.name, attr.value);
    });
    newScript.innerHTML = script.innerHTML;
    script.parentNode.appendChild(newScript);
    script.parentNode.removeChild(script);
  });
};

const hasTagsContainingWord = (word) => {
  return word.search(">[^<>]+<") !== -1
}

/**
 * Fetches a script from a given URL and executes it.
 *
 * @param {string} url - The URL of the script to fetch and execute.
 * @returns {Promise<void>} A Promise that resolves when the script has been fetched and executed.
 */
const executeScript = async (url) => {
  try {
    await fetch(url)
      .then((res) => res.text())
      .then((script) => new Function(script)());
  } catch (error) {
    console.error(`Error fetching and executing script from ${url}:`, error);
  }
}

const log = (isDebugMode, ...args) => {
  if (isDebugMode) {
    console.log(...args)
  }
}

export {
  createEvent,
  parseFromString,
  executeScriptTags,
  hasTagsContainingWord,
  DEFAULT_FLAGS,
  executeScript,
  isIntlSegmenterSupported,
  log,
};

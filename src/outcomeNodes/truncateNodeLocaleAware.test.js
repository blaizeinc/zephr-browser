import truncateNodeLocaleAware from './truncateNodeLocaleAware';

describe('truncateNodeLocaleAware tests', () => {
  beforeEach(() => {
    document.documentElement.lang = 'en';
  });

  it('empty html truncated is empty', () => {
    const emptyDiv = document.createElement('div');
    expect(truncateNodeLocaleAware(emptyDiv, { truncateLength: 100, style: 'nostyle' }))
      .toEqual(emptyDiv);
  });

  it('html shorter than truncate length is not truncated', () => {
    const original = createNodeFromHTML('<div>Hello</div>');
    const truncated = truncateNodeLocaleAware(
      original,
      { truncateLength: 4, style: 'nostyle' }
    );

    expect(truncated).toEqual(original);
  });

  it('newlines inside pre tags are maintained', () => {
    const original = createNodeFromHTML(`
      <pre>
        This is some text
        and I need my

        newlines.



        Don't show these words though ;)
      </pre>
    `);

    const expectedTruncate2 = createNodeFromHTML(`
      <pre>
        This is some text...</pre>
    `);

    const expectedTruncate5 = createNodeFromHTML(`
      <pre>
        This is some text
        and I need my

        newlines.</pre>
    `);

    // Max length is twice truncate length
    expect(truncateNodeLocaleAware(original, { truncateLength: 2, style: 'nostyle' }).outerHTML)
      .toEqual(expectedTruncate2.outerHTML);

    // Run up to the full stop but don't include the word after it with a newline in-between
    expect(truncateNodeLocaleAware(original, { truncateLength: 5, style: 'nostyle' }).outerHTML)
      .toEqual(expectedTruncate5.outerHTML);

    expect(truncateNodeLocaleAware(original, { truncateLength: 8, style: 'nostyle' }).outerHTML)
      .toBe(truncateNodeLocaleAware(original, { truncateLength: 5, style: 'nostyle' }).outerHTML);
  });

  it('newlines inside script tags are maintained', () => {
    const original = createNodeFromHTML(`
      <script>
      function() {
          window.doAThing();
          console.log("HAHA!");
          // A long comment explaining what I'm doing
          console.log("Hehe!");
      }();
      </script>
    `);

    expect(truncateNodeLocaleAware(original, { truncateLength: 9, style: 'nostyle' }).outerHTML)
      .toEqual(original.outerHTML);
  });

  it('nested scripts are maintained', () => {
    const original = createNodeFromHTML(`
      <div>
          This is a test,
          <script>
              function() {
                  window.doAThing();
              }();
          </script>
          now I am finished the test.
      </div>
    `);

    const expected = createNodeFromHTML(`
      <div>
          This is a test,
          <script>
              function() {
                  window.doAThing();
              }();
          </script>
          now I am finished...</div>
    `);

    expect(truncateNodeLocaleAware(original, { truncateLength: 4, style: 'nostyle' }).outerHTML)
      .toEqual(expected.outerHTML);
  });

  it('lorem ipsum punctuation', () => {
    const original = createNodeFromHTML('<div>Lorem ipsum dolor, sit amet! Consectetur adipiscing elit. Sed do eiusmod tempor incididunt.</div>');
    const expected = createNodeFromHTML('<div>Lorem ipsum dolor, sit amet! Consectetur adipiscing elit.</div>');

    expect(truncateNodeLocaleAware(original, { truncateLength: 6, style: 'nostyle' }).outerHTML)
      .toEqual(expected.outerHTML);
  });

  it.each([
    ['ar-SA', 3, '<div>مرحباً أيها العالم، كيف حالك</div>'],
    ['de-DE', 4, '<div>Hallo Welt,\n wie geht es dir?</div>'],
    ['en-UK', 3, '<div>Hello World,\n how are you?</div>'],
    ['es', 3, '<div>hola mundo,\n ¿cómo estás?</div>'],
    ['ja-JP', 3, '<div>こんにちは世界、お元気ですか</div>'],
    ['zh-CN', 4, '<div>你好，世界，你好吗</div>'],
  ])('locale html shorter than truncate length is not truncated %s, %i', (locale, truncateLength, original) => {
    document.documentElement.lang = locale;
    const originalNode = createNodeFromHTML(original);

    expect(truncateNodeLocaleAware(originalNode, { truncateLength, style: 'nostyle' }).outerHTML)
      .toEqual(originalNode.outerHTML);
  });

  it.each([
    [
      'ja',
      30,
      `<p>
        東京の繁華街で賑わう夜。ビルの明かりが街を照らし、人々が行き交う。ラーメン店の香りが漂い、
        居酒屋では声が飛び交う。そんな中、ひとりの若者が歩いている。
        彼は夢を追いかけ、未来への一歩を踏み出している。街の喧騒の中でも、彼の心は静かで、
        確かな目標を持っている。彼の背中には希望と勇気が宿っている。
      </p>`,
      `<p>
        東京の繁華街で賑わう夜。ビルの明かりが街を照らし、人々が行き交う。ラーメン店の香りが漂い、
        居酒屋では声が飛び交う。</p>`,
    ],
    [
      'ja',
      8,
      `<p>
        東京の繁華街で賑わう夜。ビルの明かりが街を照らし、人々が行き交う。ラーメン店の香りが漂い、
        居酒屋では声が飛び交う。そんな中、ひとりの若者が歩いている。
      </p>`,
      `<p>
        東京の繁華街で賑わう夜。ビルの明かりが街を照らし...</p>`,
    ],
  ])('locale text can be truncated %s, %i', (locale, truncateLength, original, truncated) => {
    document.documentElement.lang = locale;
    const originalNode = createNodeFromHTML(original);
    const truncatedNode = createNodeFromHTML(truncated);

    expect(truncateNodeLocaleAware(originalNode, { truncateLength, style: 'nostyle' }).outerHTML)
      .toEqual(truncatedNode.outerHTML);
  });

});

/**
 * @param {string} htmlString
 * @returns {Node}
 */
function createNodeFromHTML(htmlString) {
  const div = document.createElement('div');
  div.innerHTML = htmlString.trim();
  return div.firstElementChild;
}

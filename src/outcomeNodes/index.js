import truncateNodeLegacy from './truncateNodeLegacy';
import truncateNodeLocaleAware from './truncateNodeLocaleAware';

export {
  truncateNodeLegacy,
  truncateNodeLocaleAware,
};

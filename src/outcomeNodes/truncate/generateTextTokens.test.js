import generateTextTokens from './generateTextTokens';

const basicTestHtml = `
  <div id="test">
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      Sagittis eu volutpat odio facilisis mauris.
    </p>
  </div>
`;

const enClientExcerpt1 = `
  <section className="article-inner-content" itemProp="articleBody" id="startBannerSticky">
    <p>Among the winners of the George Polk Awards is a journalist from <em>The New York Times</em> who is accused of
      infiltrating into Israel early in the morning of the October 7 massacres.</p>
    <section className="fake-br-for-article-body"></section>
    <p>The journalist Yousef Masoud was awarded the prize along with another freelance <em>Times</em> photojournalist
      named Samar Abu Elouf.&nbsp;</p>
    <section className="fake-br-for-article-body"></section>
    <p></p>
    <script src="https://platform.twitter.com/widgets.js"></script>
    <p></p>
    <section className="fake-br-for-article-body"></section>
    <p>Honest Reporting, a <a href="https://www.redacted.com/international/article-786839" target="_blank" rel="">media
      watchdog NGO</a>, highlighted his accreditation to a photo provided to the Associated Press, with the caption,
      “Palestinians wave their national flag and celebrate by a destroyed Israeli tank at the Gaza Strip fence east of
      Khan Younis southern Saturday, Oct. 7, 2023. The militant Hamas rulers of the Gaza Strip carried out an
      unprecedented, multi-front attack on Israel at daybreak Saturday, firing thousands of rockets as dozens of Hamas
      fighters infiltrated the heavily fortified border in several locations by air, land, and sea and catching the
      country off-guard on a major holiday.”&nbsp;&nbsp;</p>
  </section>
`;

const enClientExcerpt2 = `
  <div class="single__content entry-content m-bottom entry-content-exclusive">
    <div class="single__inline-module inline-module inline-module--newsletter alignright">
      <div class="inline-module__inner">
        <div class="inline-module__icon">
          <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40"><circle cx="20" cy="20" r="20" fill="#C60800"></circle><g fill="#fff"><path d="M9 13v14.414h22V13l-11 9.1Z" data-name="envelope"></path><path d="M11.3 13h17.864l-8.933 6.909z" data-name="top flap #FFFFFF"></path></g></svg>
        </div>
        <h3 class="inline-module__title headline headline--combo-sm-md">
          Delivering insights on all things Amazin's		</h3>
        <p class="inline-module__cta">
          Sign up for Inside the Mets by Mike Puma, exclusively on Sports+		</p>
        <form class="inline-module__form newsletter-form" data-redacted-editor="newsletter-block">
          <div class="newsletter-form__wrapper">
            <div class="floating-label input-container input-container--floating-label">
              <input type="hidden" name="site_id" value="redacted">
              <input type="hidden" id="_newsletter_nonce" name="_newsletter_nonce" value="65c2572fd2"><input type="hidden" name="_wp_http_referer" value="/2022/10/14/mets-have-to-be-patient-to-collect-on-pitching-prospects/">					<input type="hidden" name="list_id" value="88882">
              <input type="email" id="newsletter-block-email-635180c46fd1f" class="floating-label__input " name="email" placeholder=" ">
              <div class="floating-label__label-wrap">
                <label for="email" class="floating-label__label">Enter your email address</label>
              </div>
            </div>
            <p class="newsletter-form__error">
              <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15"><path d="M7.5 0A7.5 7.5 0 1 0 15 7.5 7.5 7.5 0 0 0 7.5 0Zm.62 3.952-.112 4.15a.516.516 0 0 1-.508.534.516.516 0 0 1-.508-.534l-.112-4.15a.621.621 0 0 1 .62-.635.621.621 0 0 1 .625.635Zm-.62 7.082a.664.664 0 1 1 .689-.663.67.67 0 0 1-.689.663Z" fill="#e13131"></path></svg>
              <span class="error_msg">
                Please provide a valid email address.					</span>
            </p>
            <input type="submit" value="Sign Up" class="submit" data-ga-event="
              {&quot;category&quot;:&quot;Inline Newsletter Module Membership - Inside the Mets&quot;,&quot;action&quot;:&quot;Button Click&quot;,&quot;label&quot;:&quot;https:\/\/redacted.com\/2022\/10\/14\/mets-have-to-be-patient-to-collect-on-pitching-prospects\/&quot;}					">
            <div class="newsletter-form__notice">
              <p class="t-color-black m-bottom-none">
                By clicking above you agree to the 						<a href="/terms">Terms of Use</a>
                and 						<a href="/privacy">Privacy Policy</a>.
              </p>
            </div>
          </div>
          <div class="newsletter-form__success">
            <p class="inline-module__cta">
              <strong>Thank you</strong>
              <br> Enjoy this Post Sports+ exclusive newsletter!				</p>
            <div class="t-center">
              <a href="https://email.redacted.com/">
                <span>Check out more newsletters</span>
              </a>
            </div>
          </div>
          <input type="checkbox" name="redacted_phone" value="1" style="display:none !important" tabindex="-1" autocomplete="off">
        </form>
      </div>
    </div>
  </div>
`;

const jaClientExcerpt1 = `<p>じゅげむじゅげむごこうのすりきれ、かいじゃりすいぎょのすいぎょうまつ、うんらいまつうんらいまつ、くうねるところにすむところ、やぶらこうじのぶらこうじぱい、ぽぽぽぽぽぽぽー、ちりゅうぶらこうじぱいさんしょぼらんぽのぽーのぽーの、ちりゅうぶらこうじぱいさんしょぼらんぽのぽーのぽーの。</p>`;
const jaClientExcerpt2 = `<p>大谷翔平は、日本のプロ野球選手であり、2018年からMLBのロサンゼルス・エンゼルスでプレーしています。彼は投手としても打者としても非凡な才能を持ち、“二刀流“として知られています。投手としては速球や変化球を駆使し、打者としてはパワフルなバッティングを誇ります。彼の活躍は日本国内だけでなく、世界中の野球ファンに注目されています。</p>`;

describe('generateTextTokens tests', () => {
  it('Parsing is lossless', () => {
    const node = createNodeFromHTML(basicTestHtml);
    const parsed = generateTextTokens(node);

    const parsedString = parsed.map((token) => token.text).join("");
    expect(parsedString).toEqual(basicTestHtml.trim());
  });

  it.each([
    ['en', 1, 'Hello World'],
    ['en', 1, 'Hello World.'],
    ['en', 2, 'Hello! My name is "World"'],
    ['en', 1, '<div>\nHow\n do \n you \ndo?\n</div>'],
    ['en', 3, '<div>\nHello!\n my name is "World".\n <p>Nice one</p>\n </div>'],
    ['en', 1, '\n<div>\n<span>What</span>\n is\n <strong>going\n</strong> on?!\n</div>'],
    ['en', 3, '<div\n class="thing"\n id="janky"\n>\nHello!\n my name is "World".\n <p>Nice one</p> </div>'],
    ['en', 4, enClientExcerpt1],
    ['en', 4, enClientExcerpt2],
    ['ja', 1, 'こんにちは、世界'],
    ['ja', 2, 'こんにちは！私の名前は「ワールド」です'],
    ['ja', 1, jaClientExcerpt1],
    ['ja', 4, jaClientExcerpt2],
    ['el-GR', 1, 'Είναι όλα ελληνικά για μένα'],
  ])('Finds sentence ends for locale %s, %i', (locale, numberOfSentenceEnds, html) => {
    const node = createNodeFromHTML(html);
    const parsed = generateTextTokens(node, locale);
    const parsedSentenceEnds = parsed.filter((token) => token.isSentenceEnd).length;

    expect(parsedSentenceEnds).toEqual(numberOfSentenceEnds);
  });
});

/**
 * @param {string} htmlString
 * @returns {Node}
 */
function createNodeFromHTML(htmlString) {
  const parser = new DOMParser();
  const doc = parser.parseFromString(htmlString, 'text/html');
  return doc.body.firstChild;
}

import applyTruncateStyle from './applyTruncateStyle';

const basicTestHtml = `
  <div id="test">
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      Sagittis eu volutpat odio facilisis mauris.
    </p>
  </div>
`;

// Fadeout background image does not generate in Jest...
const fadeoutBasicTestHtml = `
  <div style="position: relative;"><div id="test">
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      Sagittis eu volutpat odio facilisis mauris.
    </p>
  <div style="position: absolute; height: 100%; width: 100%; bottom: 0px;"></div></div></div>
`;

const linebreakBasicTestHtml = `
  <div id="test">
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      Sagittis eu volutpat odio facilisis mauris.
    </p>
  <hr style="border: 1px solid #ebebeb;" /></div></div>
`;

describe('applyTruncateStyle tests', () => {
  it('nostyle is lossless', () => {
    const node = createNodeFromHTML(basicTestHtml);
    const styleEmpty = applyTruncateStyle(node);
    const styleNostyle = applyTruncateStyle(node, 'nostyle');

    expect(node.outerHTML).toEqual(styleEmpty.outerHTML);
    expect(node.outerHTML).toEqual(styleNostyle.outerHTML);
  });

  it('fadeout', () => {
    const styled = applyTruncateStyle(createNodeFromHTML(basicTestHtml), 'fadeout');
    const expected = createNodeFromHTML(fadeoutBasicTestHtml);

    expect(expected.outerHTML).toEqual(styled.outerHTML);
  });

  it('linebreak', () => {
    const styled = applyTruncateStyle(createNodeFromHTML(basicTestHtml), 'linebreak');
    const expected = createNodeFromHTML(linebreakBasicTestHtml);

    expect(expected.outerHTML).toEqual(styled.outerHTML);
  });
});

/**
 * @param {string} htmlString
 * @returns {Node}
 */
function createNodeFromHTML(htmlString) {
  const parser = new DOMParser();
  const doc = parser.parseFromString(htmlString, 'text/html');
  return doc.body.firstChild;
}

import applyTruncateStyle from './applyTruncateStyle';
import generateTextTokens, { TextTokenType } from './generateTextTokens';

export {
  applyTruncateStyle,
  generateTextTokens,
  TextTokenType,
};

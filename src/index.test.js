import * as Zephr from './index';

const basicTestHtml = `
  <div id="test">
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      Sagittis eu volutpat odio facilisis mauris.
    </p>
  </div>
`;

describe('createInstance Tests', () => {
  it('Zephr.createInstance() default', () => {
    const zephrInstance = Zephr.createInstance();
    expect(zephrInstance).toBeTruthy();
    expect(zephrInstance.cdnApi).toEqual('');
  });

  it('Zephr.createInstance() with CDN URL', () => {
    const zephrInstance = Zephr.createInstance('https://cdn.zephr.com');
    expect(zephrInstance).toBeTruthy();
    expect(zephrInstance.cdnApi).toEqual('https://cdn.zephr.com');
  });

  it('Zephr.createInstance() with CDN URL and fetcher', () => {
    const testFetcher = (url, options) => fetch(url, options);
    const zephrInstance = Zephr.createInstance('https://cdn.zephr.com', testFetcher);
    expect(zephrInstance).toBeTruthy();
    expect(zephrInstance.cdnApi).toEqual('https://cdn.zephr.com');
    expect(zephrInstance.fetcher).toEqual(testFetcher);
  });
});

describe('Zephr.run() Tests', () => {
  const originalFetch = global.fetch;
  const originalLog = console.log;
  const mockedLog = output => {};

  beforeEach(() => {
    document.body.innerHTML = basicTestHtml;
    delete window.Zephr;
    console.log = mockedLog;
  });

  afterEach(() => {
    global.fetch = originalFetch;
    console.log = originalLog;
  });

  it('run', async () => {
    global.fetch = (url) => {
      expect(url.startsWith('/')).toBeTruthy();
      let response = {};
      if (url === '/zephr/features') {
        response = [{
          targetType: 'CSS_SELECTOR',
          id: 'test',
          label: 'Test',
          slug: 'test',
          cssSelector: '#test',
        }];
      } else if (url === '/zephr/feature-transformations') {
        response = {
          featureResults: {
            test: {
              ruleTransformations: [
                {
                  type: 'TRUNCATE',
                  contentLength: 5,
                }
              ],
              zoneRuleTransformations: [],
            }
          },
          resources: {},
          accessDetails: {
            authenticated: false,
            accessDecisions: { 'test-ent': false },
            entitlements: {},
            credits: {},
            meters: {},
          },
        };
      }
      return Promise.resolve({ json: () => response });
    };

    const mockEventListener = jest.fn();
    document.addEventListener('zephr.browserDecisionsFinished', mockEventListener);
    const zephrInstance = await Zephr.run();
    expect(mockEventListener.mock.calls.length).toBe(1);
    expect(zephrInstance).toBeTruthy();
    expect(zephrInstance.cdnApi).toEqual('');
    expect(document.querySelector('#test p').innerHTML.trim()).toEqual('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do...');
    expect(window.Zephr.accessDetails).toEqual({
      authenticated: false,
      accessDecisions: { 'test-ent': false },
      entitlements: {},
      credits: {},
      meters: {},
    });
  });

  it('run with options', async () => {
    global.fetch = (url, options) => {
      expect(url.startsWith('https://cdn.zephr.com')).toBeTruthy();
      let response = {};
      if (url.endsWith('/zephr/features')) {
        response = [{
          targetType: 'CSS_SELECTOR',
          id: 'test',
          label: 'Test',
          slug: 'test',
          cssSelector: '#test',
        }];
      } else if (url.endsWith('/zephr/feature-transformations')) {
        expect(options.headers.Authorization).toEqual('Bearer 1234');
        const body = JSON.parse(options.body);
        expect(body.featureIds[0]).toEqual('test');
        expect(body.customData.test).toEqual('data');
        response = {
          featureResults: {
            test: {
              ruleTransformations: [
                {
                  type: 'TRUNCATE',
                  contentLength: 5,
                }
              ],
              zoneRuleTransformations: [],
            }
          },
          resources: {},
          accessDetails: {
            authenticated: true,
            accessDecisions: { 'ent-yes': true },
            entitlements: {
              'ent-yes': { usedInDecision: true },
            },
            credits: {},
            meters: {},
            trials: {
              'tracker-trial-2': {
                usedInDecision: true,
                decrementedInDecision: true,
                totalCredits: 5,
                remainingCredits: 4,
              },
            }
          },
        };
      }
      return Promise.resolve({ json: () => response });
    };

    window.Zephr = {};
    window.Zephr.accessDetails = {
      authenticated: true,
      accessDecisions: { 'trial-ent': true },
      entitlements: {},
      credits: {
        'trial-ent': {
          usedInDecision: true,
          decrementedInDecision: true,
          totalCredits: 10,
          remainingCredits: 9,
        },
      },
      trials: {
        'tracker-trial-1': {
          usedInDecision: true,
          decrementedInDecision: true,
          totalCredits: 10,
          remainingCredits: 9,
        },
      },
      meters: {},
    };

    const zephrInstance = await Zephr.run({
      cdnApi: 'https://cdn.zephr.com',
      jwt: '1234',
      customData: {
        test: 'data',
      },
    });
    expect(zephrInstance).toBeTruthy();
    expect(zephrInstance.cdnApi).toEqual('https://cdn.zephr.com');
    expect(document.querySelector('#test p').innerHTML.trim()).toEqual('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do...');
    expect(window.Zephr.accessDetails).toEqual({
      authenticated: true,
      accessDecisions: { 'ent-yes': true, 'trial-ent': true },
      entitlements: {
        'ent-yes': { usedInDecision: true },
      },
      credits: {
        'trial-ent': {
          usedInDecision: true,
          decrementedInDecision: true,
          totalCredits: 10,
          remainingCredits: 9,
        }
      },
      meters: {},
      trials: {
        'tracker-trial-1': {
          usedInDecision: true,
          decrementedInDecision: true,
          totalCredits: 10,
          remainingCredits: 9,
        },
        'tracker-trial-2': {
          usedInDecision: true,
          decrementedInDecision: true,
          totalCredits: 5,
          remainingCredits: 4,
        },
      }
    })
  });

  it('run feature response object', async () => {
    global.fetch = (url) => {
      expect(url.startsWith('/')).toBeTruthy();
      let response = {};
      if (url === '/zephr/features') {
        response = {
          features: [
            {
              targetType: 'CSS_SELECTOR',
              id: 'test',
              label: 'Test',
              slug: 'test',
              cssSelector: '#test',
            }
          ],
          datalayerOutcomesConfig: {
            datalayerName: 'testDatalayer',
            includeOutcomes: false,
            outcomesAsEvents: false,
            groupFields: false
          }
        };
      } else if (url === '/zephr/feature-transformations') {
        response = {
          featureResults: {
            test: {
              ruleTransformations: [
                {
                  type: 'TRUNCATE',
                  contentLength: 5,
                }
              ],
              zoneRuleTransformations: [],
            }
          },
          resources: {},
          accessDetails: {
            authenticated: false,
            accessDecisions: { 'test-ent': false },
            entitlements: {},
            credits: {},
            meters: {},
            trials: {
              'tracker-trial-1': {
                usedInDecision: true,
                decrementedInDecision: true,
                totalCredits: 10,
                remainingCredits: 9,
                reportInDataLayer: true,
                dataLayerCreditsUsedKey: 'trial1Used',
                dataLayerCreditsRemainingKey: 'trial1Remaining',
              }
            }
          },
        };
      }
      return Promise.resolve({ json: () => response });
    };

    const mockEventListener = jest.fn();
    document.addEventListener('zephr.browserDecisionsFinished', mockEventListener);
    const zephrInstance = await Zephr.run();
    expect(mockEventListener.mock.calls.length).toBe(1);
    expect(zephrInstance).toBeTruthy();
    expect(zephrInstance.cdnApi).toEqual('');
    expect(document.querySelector('#test p').innerHTML.trim()).toEqual('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do...');
    expect(window.Zephr.accessDetails).toEqual({
      authenticated: false,
      accessDecisions: { 'test-ent': false },
      entitlements: {},
      credits: {},
      meters: {},
      trials: {
        'tracker-trial-1': {
          usedInDecision: true,
          decrementedInDecision: true,
          totalCredits: 10,
          remainingCredits: 9,
          reportInDataLayer: true,
          dataLayerCreditsUsedKey: 'trial1Used',
          dataLayerCreditsRemainingKey: 'trial1Remaining',
        }
      }
    });
    expect(window.testDatalayer).toEqual([
      {
        event: 'zephr-pageview',
        trial1Used: 1,
        trial1Remaining: 9,
      }
    ]);
  });
});

import { parseFeatureResults } from './featureResults';

describe('parseFeatureResults', () => {
  it('should return an empty object when featureResults is undefined', () => {
    const result = parseFeatureResults(undefined);
    expect(result).toEqual({});
  });

  it('should parse feature results correctly', () => {
    const featureResults = {
      ruleTransformations: [
        { type: 'LEAVE_PRISTINE' },
        { type: 'TRUNCATE', contentLength: 10 },
      ],
      zoneRuleTransformations: [
        {
          zone: 'zone1',
          ruleTransformations: [{ type: 'FORM', id: 'form1' }],
        },
      ],
    };

    const expectedResult = {
      ruleTransformations: [
        { type: 'LeavePristine' },
        { type: 'Truncate', truncateLength: 10, style: 'nostyle' },
      ],
      zoneRuleTransformations: [
        {
          type: 'Zone',
          id: 'zone1',
          contents: [{ type: 'Form', id: 'form1' }],
        },
      ],
    };

    const result = parseFeatureResults(featureResults);
    expect(result).toEqual(expectedResult);
  });

  it('should handle all transformation types correctly', () => {
    const featureResults = {
      ruleTransformations: [
        { type: 'LEAVE_PRISTINE' },
        { type: 'TRUNCATE', contentLength: 10 },
        { type: 'TRUNCATE_WITH_STYLE', contentLength: 20, style: 'linebreak' },
        { type: 'OUTCOME_TRACKER', featureId: 'feature1', featureLabel: 'Feature 1', transformationId: 'outcome1', transformationLabel: 'Outcome 1' },
      ],
      zoneRuleTransformations: [
        {
          zone: 'zone1',
          ruleTransformations: [
            { type: 'FORM', id: 'form1' },
            { type: 'PAYMENT_FORM', id: 'paymentForm1' },
            { type: 'RESOURCE', id: 'resource1' },
            { type: 'PARAMETERISED_RESOURCE', id: 'resource2' },
            { type: 'URL', url: 'https://example.com' },
            { type: 'COMPONENT_TEMPLATE', id: 'template1' },
            { type: 'PARAMETERISED_COMPONENT_TEMPLATE', id: 'template2' },
          ],
        },
      ],
    };

    const expectedResult = {
      ruleTransformations: [
        { type: 'LeavePristine' },
        { type: 'Truncate', truncateLength: 10, style: 'nostyle' },
        { type: 'Truncate', truncateLength: 20, style: 'linebreak' },
        {
          type: 'OutcomeTracker',
          featureId: 'feature1',
          featureLabel: 'Feature 1',
          transformationId: 'outcome1',
          transformationLabel: 'Outcome 1',
        },
      ],
      zoneRuleTransformations: [
        {
          type: 'Zone',
          id: 'zone1',
          contents: [
            { type: 'Form', id: 'form1' },
            { type: 'PaymentForm', id: 'paymentForm1' },
            { type: 'UIComponent', componentId: 'resource1' },
            { type: 'UIComponent', componentId: 'resource2' },
            { type: 'HostedUIComponent', url: 'https://example.com' },
            { type: 'ComponentTemplate', templateId: 'template1' },
            { type: 'PARAMETERISED_COMPONENT_TEMPLATE', templateId: 'template2' },
          ],
        },
      ],
    };

    const result = parseFeatureResults(featureResults);
    expect(result).toEqual(expectedResult);
  });

  it('should handle missing ruleTransformations and zoneRuleTransformations', () => {
    const featureResults = {};

    const expectedResult = {
      ruleTransformations: [],
      zoneRuleTransformations: [],
    };

    const result = parseFeatureResults(featureResults);
    expect(result).toEqual(expectedResult);
  });
});

# Zephr Browser

A JavaScript library to handle Zephr decisions in the browser.

## Usage

### UMD Module

```html
<script src="https://assets.zephr.com/zephr-browser/<VERSION>/zephr-browser.umd.js">
<script>
  zephrBrowser.run();
</script>
```

### ES Module

```js
import * as zephrBrowser from 'https://assets.zephr.com/zephr-browser/<VERSION>/zephr-browser.esm.js';
zephrBrowser.run();
```

## Run method optional arguments

The run method allows for an optional arguments object containing the following:

- cdnApi \[String\] - Zephr CDN API base url
- jwt \[String\] - JWT token for authentication
- customData \[Object\] - Additional string key/values to be used in decisions
- debug \[Boolean\] - Enables debug logging
- fetcher \[Function\] - The fetch function to be used for making requests. If not provided, the default fetch is used.
- automaticPolyfills \[Boolean\] - Enables automatic polyfills for unsupported browser APIs, currently for
                                  `Intl.Segmenter`. Defaults to `true`.

## Debugging


To enable debug logging when calling `zephrBrowser.run()` add the optional argument to options within `run()`:

```js
import * as zephrBrowser from '@zephr/browser';
zephrBrowser.run({
  customData: {
	  counter: 40,
	  anything: true,
	  ref: document.referrer
  },
  jwt: "...",
  debug: true,
  fetcher: (url, options) => {
    return fetch(url, options);
  }
});
```

If you're unable to manually call `zephrBrowser.run()`, you can instead set a localstorage item `zephrBrowserDebug` to `true` to enable debug logging:

- Press f12 in the browser
- Click on the Application tab
- Click on Local Storage
- Click on the domain of the site you're on
- Add a new item with the key `zephrBrowserDebug` and the value `true`


## Browser events triggered by Zephr

- `zephr.browserDecisionsFinished` - is triggered at the end of the call to `run()`

## Building and running on localhost

First install dependencies:

```sh
npm install
```

To develop locally make sure you are forwarding <http://localhost:3003> through a local Zephr Site and run:

```sh
npm start
```

To create a build:

```sh
npm run build
```
